//
//  MessageFile.swift
//  File Me
//
//  Created by Lê Dũng on 3/10/19.
//

import UIKit


enum FileNotify : String
{
    case fileList           = "FileList"
    case fileCreate         = "FileCreate"
    case fileDelete         = "FileDelete"
}

